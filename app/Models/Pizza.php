<?php

namespace App\Models;

use App\Models\Ingredient;
use Illuminate\Database\Eloquent\Model;

class Pizza extends Model
{
    protected $fillable = ['name', 'sell_price'];

    protected $ingredientsInStock;

    public function ingredients(){
        return $this->belongsToMany(Ingredient::class)->withTimestamps();
    }
}
