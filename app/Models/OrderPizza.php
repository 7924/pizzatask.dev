<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderPizza extends Model
{
    protected $table = 'order_pizza';

    public function ingredients(){
        return $this->hasMany(OrderPizzaIngredient::class);
    }

    public function pizza(){
        return $this->hasOne(Pizza::class, 'id', 'pizza_id');
    }
}
