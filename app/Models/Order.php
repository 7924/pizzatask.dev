<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function pizzas(){
        return $this->belongsToMany(Pizza::class, 'order_pizza')->withTimestamps();
    }

    public function  orderedPizzas(){
        return $this->hasMany(OrderPizza::class);
    }
}
