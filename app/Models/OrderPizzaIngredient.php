<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderPizzaIngredient extends Model
{
    public function ingredient(){
        return $this->hasOne(Ingredient::class,'id', 'ingredient_id');
    }
}
