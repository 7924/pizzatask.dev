<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IngredientPizza extends Model
{
    protected $table = 'ingredient_pizza';
}
