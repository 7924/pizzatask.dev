<?php

namespace App\Http\Controllers\api;

use App\Models\Ingredient;
use App\Models\Order;
use App\Models\OrderPizza;
use App\Models\OrderPizzaIngredient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{

    /**
     * @var int
     */
    private $sellingPrice = 0;

    private $ingredientsArray = [];

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function makeOrder(Request $request){

        if(!$request->has('pizza')){
            return response()->json(['message'=>'missing data!']);
        }

        $order = new Order();
        $order->user_id = 1;
        $order->save();

        $orderPizza = new OrderPizza();

        $pizza = $request->get('pizza');
        $orderPizza->order_id = $order->id;
        $orderPizza->pizza_id = $pizza['id'];

        $orderPizza->save();

        $ingredientArray = [];

        foreach($pizza['ingredients'] as $ingredient){

            $this->ingredientsArray[] = $ingredient['id'];

            $ingredientArray[] = [
                'order_pizza_id' => $orderPizza->id,
                'ingredient_id' => $ingredient['id'],
            ];


        }

        $this->calculateSellingPrice();

        OrderPizzaIngredient::insert($ingredientArray);

        $orderPizza->sell_price = $this->sellingPrice;

        $orderPizza->save();

        $order->load(['orderedPizzas'=> function($q) {
            $q->with(['ingredients'=>function($q){
                $q->orderBy('id', 'asc')->with('ingredient');
            }, 'pizza']);
        }]);

        return response()->json(['message' => $order], 201);

    }

    public function getOrder($id){
        $order = Order::with(['orderedPizzas'=> function($q) {
            $q->with(['ingredients'=>function($q){
                $q->orderBy('id', 'asc')->with('ingredient');
            }, 'pizza']);
        }])->find($id);

        return response()->json(['message' => $order], 201);
    }


    /**
     *
     */
    private function calculateSellingPrice(){
        $ingredients = Ingredient::whereIn('id', $this->ingredientsArray)->get();

        $ingredients->each(function($ingredient){
            $this->sellingPrice +=  $ingredient->cost_price;
        });

        $this->sellingPrice += $this->sellingPrice * env('PREPARATION_RATE_ON_material_COSTS') / 100;

        return;
    }

}
