<?php

namespace App\Http\Controllers\Api;

use App\Models\Ingredient;
use App\Models\IngredientPizza;
use App\Models\Pizza;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PizzaController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $pizzas = Pizza::all();
        return response()->json(['pizzas' => $pizzas], 200);
    }


    public function get($id){

        $pizza =  Pizza::with('ingredients')->find($id);

        $pizza->ingredientsInStock = Ingredient::whereNotIn('id', $pizza->ingredients->pluck('id')->toArray() )->get();

        return $pizza;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {

        $pizza = Pizza::create($request->all());
        return response()->json(['pizza' => $pizza], 201);
    }


    public function update(Request $request, $id)
    {
        $pizza = Pizza::find($id);
        if(!$pizza){
            return response()->json(['message'=> 'Pizza not found'], 404);
        }
        $pizza->update($request->all());
        return respnse()->json(['pizza'=>$pizza], 200);
    }

    public function destroy($id)
    {
        $pizza = Pizza::find($id);
        $pizza->delete();
        return response()->json(['message'=>'Pizza deleted'], 200);
    }


}
