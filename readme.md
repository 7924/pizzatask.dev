Laravel & Vue.js

Task:

The application will allow the users to manage a small catalog of pizzas and the ingredients needed to bake them. 
In particular, users must be able to add and remove the ingredients of an existing pizza (and if possible give those ingredients an order of appearance).

- Ingredients (An ingredient has a name and a cost price.)
- Pizzas (A pizza has a name, a selling price and is made from several ingredients)

The selling price of a pizza equals the total of all its ingredients plus 50% ?of the total for the preparation.


Examples:

- The "MacDac Pizza" is made of the following ingredients:

1. tomato ­ 0.5 eur
2. sliced mushrooms ­ 0.5 eur
3. feta cheese ­ 1.0 eur
4. sausages ­ 1.0 eur
5. sliced onion ­ 0.5 eur
6. mozzarella cheese ­ 0.3 eur
7. oregano 2 eur

Total Price = 8.3 eur

- The "Lovely Mushroom Pizza" has a layer of mushrooms on top, and is made of

these ingredients:

1. tomato ­ 0.5 eur
2. bacon ­ 1.0 eur
3. mozzarella cheese ­ 0.3 eur
4. sliced mushrooms ­ 0.5 eur
5. oregano ­ 2.0 eur

Total Price = 6.05 eur



Technology usage

* Please use Laravel 5.x version
* For frontend use bootstrap 3.x version and VueJS
* Do not use jQuery!
* Please use bitbucket or github (public)
* MySQL or MariaDB
* 

Demo: http://pizza.testavide.tk