<html>
<hrad>
    <head>
        <script>

            window.Laravel =  <?php echo json_encode(['csrfToken' => csrf_token(),]); ?>
        </script>
    </head>
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    <style>
        #app {
            margin: 30px;
        }
        .nav{
            margin-bottom: 30px;
        }

    </style>
</hrad>


<body>


<div id='app'>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul class="nav nav-tabs">
                    <li role="presentation"><router-link to="/">Pizzas</router-link></li>
                    <li role="presentation"><router-link to="/new-pizza">New Pizza</router-link></li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <router-view></router-view>
            </div>
        </div>
    </div>


</div>


<script type="text/javascript" src="{{ mix('/js/app.js') }}"></script>
</body>
</html>