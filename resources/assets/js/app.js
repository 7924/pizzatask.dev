
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// var Vue = require('vue');

Vue.component('example', require('./components/Example.vue'));
Vue.component('app-pizzas', require('./components/pizzas.vue'));
Vue.component('app-new-pizza', require('./components/new-pizza.vue'));

import Pizzas from './components/pizzas.vue';
import NewPizza from './components/new-pizza.vue';
import CustomizePizza from './components/customize-pizza.vue';
import Order from './components/order.vue';

Vue.use(VueRouter);


let routes = [
    {path: '/', component: Pizzas},
    {path: '/new-pizza', component: NewPizza},
    {path: '/customize-pizza/:id/', name:'customizepizza', component: CustomizePizza},
    {path: '/order/:id', name:'order', component: Order},
    {path: '/order', name:'order-find', component: Order}

];

const router = new VueRouter({
    mode: 'history',
    routes: routes
});

const app = new Vue({
    el: '#app',
    // render: h => h(app),
    router: router
});


