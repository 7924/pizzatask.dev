<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['prefix'=>'pizza', 'namespace'=> 'api'], function(){
    Route::get('/', 'PizzaController@index');
    Route::get('{id}/get', 'PizzaController@get');
    Route::post('/', 'PizzaController@store');
    Route::put('/{id}', 'PizzaController@update');
    Route::delete('/{id}', 'PizzaController@destroy');
    
    
    Route::post('make-order', 'OrderController@makeOrder');
    Route::get('get-order/{id}', 'OrderController@getOrder');

});


